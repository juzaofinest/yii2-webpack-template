<?php
require __DIR__ . '/../../api/env.php';
require __DIR__ . '/../../api/vendor/yiisoft/yii2/Yii.php';

$config = yii\helpers\ArrayHelper::merge(
	require (__DIR__ . '/../../api/config/common.php'),
	require (__DIR__ . '/../../api/config/web.php')
);

(new yii\web\Application($config))->run();
