<?php

return [
  'components'     => [
    'authManager' => [
      'class' => 'yii\rbac\PhpManager',
    ],
    'formatter'   => [
      'timeZone'        => 'Europe/Moscow',
      'defaultTimeZone' => 'Europe/Moscow',
    ],
    'mailer'      => [
      'class'            => 'yii\swiftmailer\Mailer',
      'useFileTransport' => true,
    ],
    'urlManager'  => [
      'enablePrettyUrl' => true,
      'showScriptName'  => false,
    ],
    'view'        => [
      'renderers' => [
        'jade' => [
          'class' => 'app\components\JadeViewRenderer',
          // 'cachePath' => '@runtime/Jade/cache',
          // 'options'   => [
          //   'pretty'   => true,
          //   'lifeTime' => 0, //3600 -> 1 hour
          // ],
        ],
      ],
    ],
  ],
  'sourceLanguage' => 'ru-RU',
  'language'       => 'ru-RU',
];
