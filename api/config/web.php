<?php

$params = require __DIR__ . '/params.php';

$config = [
  'id' => 'twiger',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'components' => [
    'assetManager' => [
      'converter'=> [
//         'forceConvert' => true,
        'commands' => [
          'styl' => ['css', '../../node_modules/stylus/bin/stylus --include-css < {from} > {to}'],
        ],
      ],
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'db' => require __DIR__ . '/db.php',
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'mailer' => [
      'class' => 'yii\swiftmailer\Mailer',
      'useFileTransport' => true,
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'request' => [
      'cookieValidationKey' => $_SERVER['COOKIE_KEY'],
      'parsers' => [
        'application/json' => 'yii\web\JsonParser',
      ],
    ],
    'user' => [
      'identityClass' => 'app\models\User',
      'enableAutoLogin' => true,
    ],
  ],
  'layout' => 'main',
  'params' => $params,
];

if (YII_ENV_DEV) {
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
  ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
  ];
}

return $config;
