<?php
date_default_timezone_set('Europe/Moscow');

require __DIR__ . '/vendor/autoload.php';

function dump() {
	$a = func_get_args();
	if (count($a) == 1) {
		$a = $a[0];
	}
	echo '<pre>';
	print_r($a);
}

function vdump() {
	$a = func_get_args();
	if (count($a) == 1) {
		$a = $a[0];
	}
	echo '<pre>';
	var_dump($a);
}

function edump() {
	dump(func_get_args());
	exit;
}

function evdump() {
	vdump(func_get_args());
	exit;
}

function ddump($a) {
	file_put_contents('data.txt', '---' . PHP_EOL . date('H:i:s') . PHP_EOL . print_r($a, 1) . PHP_EOL, FILE_APPEND);
}

$envfile = '.env';

/*
if ( ($_SERVER["HTTP_HOST"] === 'local.bubujka.org:3000') || (!isset($_SERVER["HTTP_HOST"])) ) {
$envfile = '.env.development';
} elseif ($_SERVER["HTTP_HOST"] === 'test.huskyhockeyteam.com') {
$envfile = '.env.production';
}
 */

do {
	if (file_exists(__DIR__ . '/' . $envfile)) {
		$de = new Dotenv\Dotenv(__DIR__, $envfile);
		break;
	}

	if (file_exists(__DIR__ . '\..\\' . $envfile)) {
		$de = new Dotenv\Dotenv(__DIR__ . '/../', $envfile);
		break;
	}

	if (file_exists(__DIR__ . '/../' . $envfile)) {
		$de = new Dotenv\Dotenv(__DIR__ . '/../', $envfile);
		break;
	}
} while (false);

if (isset($de)) {
	$de->overload();
	$de->required([
		'YII_ENV',
		'MYSQL_HOST', 'MYSQL_USER', 'MYSQL_PASSWORD', 'MYSQL_DATABASE',
		'COOKIE_KEY',
	]);
}

if ('dev' == $_SERVER['YII_ENV']) {
	define('YII_ENV', 'dev');
	define('YII_DEBUG', true);
} else {
	define('YII_ENV', 'prod');
	define('YII_DEBUG', false);
}
