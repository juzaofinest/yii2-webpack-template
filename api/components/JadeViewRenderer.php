<?php

namespace app\components;

class JadeViewRenderer extends \yii\base\ViewRenderer {
  public function render($view, $file, $params) {
    exec('jade ' . $file . ' -P -E php');
    $file = preg_replace('/jade$/', 'php', $file);
    // edump($view);

    return $view->render('index.php', $params);
  }
}