<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\db\Exception;

class DbController extends Controller {
  public function actionRecreate() {
    $this->actionDrop();
    $this->actionCreate();
  }

  public function actionCreate() {
    $connection = Yii::$app->db;
    $connection->dsn = 'mysql:host=' . $_SERVER['MYSQL_HOST'];
    try {
      $connection->createCommand('CREATE DATABASE `' . $_SERVER['MYSQL_DATABASE'] . '`')->execute();
      echo 'БД ' . $_SERVER['MYSQL_DATABASE'] . ' создана' . PHP_EOL;
    } catch (Exception $e) {
      echo 'Ошибка создания базы' . PHP_EOL;
      echo $e->getMessage();
    }
    echo PHP_EOL;
    return 0;
  }

  public function actionDrop() {
    $connection = Yii::$app->db;
    $connection->dsn = 'mysql:host=' . $_SERVER['MYSQL_HOST'];
    try {
      $connection->createCommand('DROP DATABASE `' . $_SERVER['MYSQL_DATABASE'] . '`')->execute();
      echo 'БД ' . $_SERVER['MYSQL_DATABASE'] . ' удалена' . PHP_EOL;
    } catch (Exception $e) {
      echo 'Ошибка удаления базы' . PHP_EOL;
      echo $e->getMessage();
    }
    echo PHP_EOL;
    return 0;
  }

}
