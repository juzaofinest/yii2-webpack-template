'use strict';
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WebpackErrorNotificationPlugin = require('webpack-error-notification');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var OpenBrowserPlugin = require('open-browser-webpack-plugin');
var path = require('path');
var autoprefixer = require('autoprefixer');
var NODE_ENV = process.env.NODE_ENV === 'production' ? 'production': 'development';

var config = {
  entry: {
    frontend: './frontend/index.js'
  },
  output: {
    path: __dirname + '/www/',
    filename: 'build/[name].js?[hash]'
  },
  resolve: {
    modulesDirectories: [
      './node_modules/',
      './frontend/_common/',
    ],
    extensions: ['', '.js', '.css']
  },

  resolveLoader: {
    modulesDirectories: ['./node_modules/'],
    moduleTemplates:    ['*-loader', '*'],
    extensions:         ['', '.js'],
  },

  devServer: {
    contentBase: __dirname + '/www',
    watchOptions: {
      aggregateTimeout: 100,
      poll: 1000
    },
    proxy: {
      '/api/*': 'http://127.0.0.1:3001',
    },
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, '/admin'),
        exclude: /\/(node_modules|bower_components)\//,
        loader: 'es3ify!ng-annotate!babel',
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.html$/,
        loader: 'html'
      },
      {
        test:   /\.jade$/,
        loader: 'html?attrs=img:src xlink:href source:src image:xlink:href!jade-html',
      },
      {
        test:   /\.styl$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!postcss!stylus?sourceMap')
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!postcss'),
      },
      {
        test:   /\.(png|svg|jpg|gif|ttf|eot|woff|woff2|webm|ogv|mp4)$/,
        loader: 'file?name=/assets/[hash].[ext]',
      }
    ],

    noParse: [
      /angular.min.js/,
      /jquery.js/,
      /angular.js/,
    ],
  },

  stylus : {
    import: [
      '~stylus-mixins/index.styl',
    ]
  },

  postcss: function(){
    return [
      autoprefixer({
        browsers: ['last 3 version']
      })
    ];
  },

  devtool: NODE_ENV === 'production' ? null : 'inline-source-map',

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Pockerstars',
      template: 'frontend/index.jade',
      filename: 'index.html',
      chunks: ['frontend']
    }),
    new ExtractTextPlugin('[name].css?[hash]', {
      allChunks: false,
    }),
  ]
};

if(NODE_ENV === 'production') {

  config.plugins.push(

    new ProgressBarPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings:     false,
        drop_console: true,
        unsafe:       true
      }
    })
  );

} else {
  module.exports.plugins = module.exports.plugins.concat([
    new WebpackErrorNotificationPlugin(),
    new webpack.NoErrorsPlugin(),
    new OpenBrowserPlugin({ url: 'http://local.bubujka.org:3000' }),
  ]);
}
module.exports = config;